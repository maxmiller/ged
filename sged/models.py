from django.db import models
from django.utils import models
# Create your models here.
class Instituicao(models.Model):
    nome = models.CharField(max_length=200)
    sigla = models.CharField(max_length=4)
    data_cadastro = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return "%s -  %s" % self.nome , self.sigla

class Unidade(models.Model):
    nome = models.CharField(max_length=200)
    sigla = models.CharField(max_length=4)
    data_cadastro = models.DateTimeField(default=timezone.now)
    instituicao = models.ForeignKey(Instituicao)

    def __str__(self):
        return self.nome

class Setor(models.Model):
    nome = models.CharField(max_length=200)
    sigla = models.CharField(max_length=10)
    data_cadastro = models.DateTimeField(default=timezone.now)
    unidade = models.ForeignKey(Unidade)
    chefe = models.ForeignKey(Servidor)

    def __str__(self):
        return self.nome


class Conselho(models.Model):
    nome = models.CharField(max_length=200)
    sigla = models.CharField(max_length=10)
    data_cadastro = models.DateTimeField(default=timezone.now)
    unidade = models.ForeignKey(Unidade)
    presidente = models.ForeignKey(Servidor)
    def __str__(self):
        return self.nome

class Servidor(models.Model):
    nome = models.CharField(max_length=200)
    siape = models.CharField(max_length=7)
    funcao = models.ForeignKey(Funcao)
    def __str__(self):
        return self.nome

class Funcao(models.Model):
    sigla = models.CharField(max_length=3)
    descricao = models.CharField(max_length=200)
    def __str__(self):
        return self.nome
